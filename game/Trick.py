from typing import List

from game_interfaces.CardInterface import CardInterface
from game_interfaces.TrickInterface import TrickInterface


class Trick(TrickInterface):
    def __init__(self, starting_player: int, number_of_players: int):
        super().__init__(starting_player, number_of_players)
        self._cards: List[CardInterface] = list()
        self._starting_player = starting_player
        self._current_player = starting_player
        self._possible_winner = None
        self._number_of_players = number_of_players

    def cards(self) -> List[str]:
        return [card.string() for card in self._cards]

    def size(self) -> int:
        return len(self._cards)

    def reset(self):
        self._cards = list()
        self._possible_winner = None
        self._current_player = self._starting_player

    def play_normal(self, chosen_card: CardInterface) -> bool:
        for card in self._cards:
            if not chosen_card.is_ge(card):
                return False
        self._cards.append(chosen_card)
        self._possible_winner = self._current_player
        self._next_player()
        return True

    def _next_player(self):
        self._current_player = (self._current_player + 1) % self._number_of_players

    def play_as_lowest(self, chosen_card: CardInterface) -> bool:
        self._cards.append(chosen_card)
        self._next_player()
        if self._possible_winner is None:
            self._possible_winner = self._current_player
        return True

    def winner(self) -> int:
        if self.size() == self._number_of_players:
            return self._possible_winner
        else:
            return None
