import itertools
import random
from typing import Type, List

from game_interfaces.CardFactoryInterface import CardFactoryInterface
from game_interfaces.CardInterface import CardInterface
from game_interfaces.DealerInterface import DealerInterface
from game_interfaces.GameInterface import GameInterface
from game_interfaces.GameStateInterface import GameStateInterface
from game_interfaces.HandInterface import HandInterface
from game_interfaces.TrickInterface import TrickInterface


class Dealer(DealerInterface):
    def __init__(self, number_of_players, hand_type: Type[HandInterface], game_type: Type[GameInterface],
                 card_type: Type[CardInterface], trick_type: Type[TrickInterface],
                 card_factory: Type[CardFactoryInterface], game_state_type: Type[GameStateInterface]):
        self._trick_type = trick_type
        self._card_type = card_type
        self._game_type = game_type
        self._hand_type = hand_type
        self._game_state_type = game_state_type
        self._number_of_players = number_of_players
        self._card_factory = card_factory(self._card_type)
        self._available_cards = list(itertools.chain.from_iterable([["2", "3", "4", "5", "6",
                                                                     "7", "8", "9", "10", "J",
                                                                     "Q", "K", "A"]
                                                                    for _ in range(4)]))

    def game_type(self) -> str:
        return "normal"

    def create_game(self) -> GameInterface:
        hands = list()
        for i in range(self._number_of_players):
            hands.append(self._create_hand())
        return self._game_type(self._number_of_players, 0, hands, self._create_trick(),
                               self._create_game_state(hands))

    def _create_game_state(self, hands: List[HandInterface]) -> GameStateInterface:
        game_state = self._game_state_type(self._number_of_players)
        for player_no in range(len(hands)):
            for card_str in hands[player_no].cards():
                game_state.add_card(player_no, card_str)
        return game_state

    def _create_hand(self) -> HandInterface:
        card_list = list()
        for i in range(6):
            random_card = random.choice(self._available_cards)
            self._available_cards.remove(random_card)
            card_list.append(random_card)
        return self._hand_type(card_list, self._card_factory)

    def _create_trick(self) -> TrickInterface:
        return self._trick_type(0, self._number_of_players)
