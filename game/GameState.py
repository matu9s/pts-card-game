from typing import List

from game_interfaces.GameStateInterface import GameStateInterface


class GameState(GameStateInterface):
    def __init__(self, number_of_players: int):
        super().__init__(number_of_players)
        self._cards = [["" for _ in range(6)] for _ in range(number_of_players)]
        self._trick = ["" for _ in range(number_of_players)]
        self._number_of_players = number_of_players

    def add_card(self, player: int, card: str):
        index = self._cards[player].index("")
        self._cards[player][index] = card

    def remove_card(self, player: int, card: str):
        index = self._cards[player].index(card)
        self._cards[player][index] = ""

    def set_trick(self, player: int, card: str):
        self._trick[player] = card

    def get_trick(self, player: int):
        return self._trick[player]

    def get_cards(self, player: int) -> List[str]:
        return self._cards[player]

    def set_others_unknown(self, player: int):
        for other_player in range(self._number_of_players):
            if other_player == player:
                continue
            else:
                number_of_empty = self._cards[other_player].count("")
                self._cards[other_player] = ["?" for _ in range(6 - number_of_empty)] + ["" for _ in
                                                                                         range(number_of_empty)]
