from typing import Type

from game_interfaces.CardFactoryInterface import CardFactoryInterface
from game_interfaces.CardInterface import CardInterface


class CardFactory(CardFactoryInterface):
    def __init__(self, card_type: Type[CardInterface]):
        super().__init__(card_type)
        self.card_type = card_type

    def create(self, card_name: str) -> CardInterface:
        return self.card_type(card_name)
