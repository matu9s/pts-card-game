from typing import List

from game_interfaces.GameInterface import GameInterface
from game_interfaces.GameStateInterface import GameStateInterface
from game_interfaces.HandInterface import HandInterface
from game_interfaces.TrickInterface import TrickInterface


class Game(GameInterface):
    def __init__(self, number_of_players: int, trick_started_by: int,
                 hands: List[HandInterface], trick: TrickInterface,
                 game_state: GameStateInterface):
        super().__init__(number_of_players, trick_started_by, hands, trick, game_state)
        self.number_of_players = number_of_players
        self.trick_started_by = trick_started_by
        self._hands = hands
        self._trick = trick
        self._state = game_state
        self.current_player = trick_started_by

    def state(self) -> GameStateInterface:
        return self._state

    def play(self, player: int, cardno: int) -> bool:
        card_played = self._hands[player].cards()[cardno]
        if player != self.current_player:
            return False
        successful = self._hands[player].play(cardno, self._trick)
        if successful:
            self._state.set_trick(player, card_played)
            self._state.remove_card(player, card_played)
            self.current_player = (self.current_player + 1) % self.number_of_players
            if self._trick.size() == self.number_of_players:
                self.current_player = self._trick.winner()
                self.trick_started_by = self.current_player
                self._new_trick()
            return True
        else:
            return False

    def _new_trick(self):
        for i in range(self.number_of_players):
            self._state.set_trick(i, "")
        self._trick.reset()
