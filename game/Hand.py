from typing import List

from game_interfaces.CardFactoryInterface import CardFactoryInterface
from game_interfaces.CardInterface import CardInterface
from game_interfaces.HandInterface import HandInterface
from game_interfaces.TrickInterface import TrickInterface


class Hand(HandInterface):
    def __init__(self, card_list: List[str], card_factory: CardFactoryInterface):
        super().__init__(card_list, card_factory)
        self._cards = list()
        self.card_factory = card_factory
        for card in card_list:
            self._cards.append(card_factory.create(card))

    def cards(self) -> List[str]:
        return [card.string() for card in self._cards]

    def play(self, cardno: int, trick: TrickInterface) -> bool:
        chosen_card = self._cards[cardno]
        if chosen_card.string() == "":
            return False
        if self._is_smallest(chosen_card):
            if trick.play_as_lowest(chosen_card):
                self._cards[cardno] = self.card_factory.create("")
                return True
            return False
        else:
            if trick.play_normal(chosen_card):
                self._cards[cardno] = self.card_factory.create("")
                return True
            return False

    def _is_smallest(self, chosen_card: CardInterface) -> bool:
        for card in self._cards:
            if card.string() == "":
                continue
            if chosen_card.is_ge(card) and not card.is_ge(chosen_card):
                return False
        return True
