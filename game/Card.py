from game_interfaces.CardInterface import CardInterface


class Card(CardInterface):
    def __init__(self, name: str):
        super().__init__(name)
        self._name = name

    def string(self) -> str:
        return self._name

    def is_ge(self, other: 'CardInterface') -> bool:
        order = {"2": 1, "3": 2, "4": 3, "5": 4, "6": 5, "7": 6, "8": 7,
                 "9": 8, "10": 9, "J": 10, "Q": 11, "K": 12, "A": 13}
        return order[self._name] >= order[other.string()]
