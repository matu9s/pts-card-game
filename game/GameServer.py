import copy

from game_interfaces.DealerInterface import DealerInterface
from game_interfaces.GameServerInterface import GameServerInterface
from game_interfaces.GameStateInterface import GameStateInterface


class GameServer(GameServerInterface):
    def __init__(self, dealer: DealerInterface):
        self._dealer = dealer
        self._game = None

    def play(self, player: int, cardno: int) -> bool:
        if self._game is None:
            return False
        return self._game.play(player, cardno)

    def state(self, player: int) -> GameStateInterface:
        state = copy.deepcopy(self._game.state())
        state.set_others_unknown(player)
        return state

    def new_game(self, type: str) -> bool:
        if self._dealer is None or self._dealer.game_type() != type:
            return False
        else:
            self._game = self._dealer.create_game()
            return True

    def change_dealer(self, dealer: DealerInterface):
        self._dealer = dealer
