import random
from typing import Type

from game.Dealer import Dealer
from game_interfaces.CardFactoryInterface import CardFactoryInterface
from game_interfaces.CardInterface import CardInterface
from game_interfaces.GameInterface import GameInterface
from game_interfaces.GameStateInterface import GameStateInterface
from game_interfaces.HandInterface import HandInterface
from game_interfaces.TrickInterface import TrickInterface


class SubsetDealer(Dealer):
    def __init__(self, number_of_players, hand_type: Type[HandInterface], game_type: Type[GameInterface],
                 card_type: Type[CardInterface], trick_type: Type[TrickInterface],
                 card_factory: Type[CardFactoryInterface], game_state_type: Type[GameStateInterface]):
        super().__init__(number_of_players, hand_type, game_type, card_type, trick_type, card_factory, game_state_type)
        self._available_cards = ["1", "2", "3", "4", "5", "6", "7"]

    def game_type(self) -> str:
        return "subset"

    def _create_hand(self) -> HandInterface:
        card_list = list()
        for i in range(6):
            random_card = ", ".join(random.sample(self._available_cards, random.randint(1, 7)))
            card_list.append(random_card)
        return self._hand_type(card_list, self._card_factory)
