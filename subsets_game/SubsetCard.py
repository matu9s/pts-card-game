from game_interfaces.CardInterface import CardInterface


class SubsetCard(CardInterface):
    def __init__(self, name: str):
        super().__init__(name)
        self._name = name
        self._card_set = set(self._name.split(", "))

    def string(self) -> str:
        return self._name

    def is_ge(self, other: 'CardInterface') -> bool:
        if set(other.string().split(", ")).issubset(self._card_set):
            return True
        else:
            return False
