import unittest
from unittest.mock import Mock

from equal_game.EqualTrick import EqualTrick
from game.Card import Card
from game.CardFactory import CardFactory
from game.Dealer import Dealer
from game.Game import Game
from game.GameServer import GameServer
from game.GameState import GameState
from game.Hand import Hand
from game.Trick import Trick
from subsets_game.SubsetCard import SubsetCard
from subsets_game.SubsetDealer import SubsetDealer


class TestCard(unittest.TestCase):
    def setUp(self):
        self.cardA = Card('A')
        self.card2 = Card('2')
        self.cardJ = Card('J')
        self.card10 = Card('10')
        self.card2b = Card('2')
        self.cardK = Card('K')

    def test_string(self):
        self.assertEqual(self.cardA.string(), 'A')
        self.assertEqual(self.card10.string(), '10')
        self.assertEqual(self.cardK.string(), 'K')

    def test_is_ge(self):
        self.assertFalse(self.card2.is_ge(self.cardA))
        self.assertTrue(self.cardA.is_ge(self.card2))
        self.assertTrue(self.card10.is_ge(self.card2))
        self.assertFalse(self.card2.is_ge(self.card10))
        self.assertTrue(self.card2b.is_ge(self.card2))
        self.assertTrue(self.card2.is_ge(self.card2b))
        self.assertTrue(self.cardK.is_ge(self.cardJ))
        self.assertFalse(self.cardJ.is_ge(self.cardK))


class TestHand(unittest.TestCase):
    def setUp(self):
        self.card_factory = CardFactory(Card)
        self.empty_hand = Hand(["" for _ in range(6)], self.card_factory)
        self.full_hand = Hand(["A", "J", "2", "K", "10", "Q"], self.card_factory)

    def test_cards(self):
        self.assertEqual(self.empty_hand.cards(), ["" for _ in range(6)])
        self.assertEqual(self.full_hand.cards(), ["A", "J", "2", "K", "10", "Q"])

    def test_play(self):
        trick_mock = Mock()
        for i in range(5):
            self.assertFalse(self.empty_hand.play(i, trick_mock))
            trick_mock.play_as_lowest.assert_not_called()
            trick_mock.play_normal.assert_not_called()
            trick_mock.reset_mock()

        trick_mock.play_as_lowest.return_value = True
        self.assertTrue(self.full_hand.play(2, trick_mock))
        trick_mock.play_as_lowest.assert_called()
        trick_mock.play_normal.assert_not_called()
        trick_mock.reset_mock()

        trick_mock.play_normal.return_value = False
        self.assertFalse(self.full_hand.play(1, trick_mock))
        trick_mock.play_normal.assert_called()
        trick_mock.play_as_lowest.asser_not_called()


class TestTrick(unittest.TestCase):
    def setUp(self):
        self.trick4 = Trick(2, 4)
        self.hand4_0 = Hand(["2", "5", "J", "4", "K", ""], CardFactory(Card))
        self.hand4_1 = Hand(["2", "2", "2", "2", "5", "10"], CardFactory(Card))
        self.hand4_2 = Hand(["4", "4", "K", "Q", "J", "10"], CardFactory(Card))
        self.hand4_3 = Hand(["4", "4", "K", "Q", "J", "10"], CardFactory(Card))

        self.trick2 = Trick(0, 2)
        self.hand2_0 = Hand(["2", "5", "J", "", "", ""], CardFactory(Card))
        self.hand2_1 = Hand(["3", "K", "3", "", "", ""], CardFactory(Card))

    def test_trick(self):
        self.assertTrue(self.trick4.play_normal(Card("J")))
        self.assertEqual(self.trick4.cards(), ["J"])
        self.assertFalse(self.trick4.play_normal(Card("5")))
        self.assertTrue(self.trick4.play_normal(Card("Q")))
        self.assertEqual(self.trick4.cards(), ["J", "Q"])
        self.assertTrue(self.trick4.play_as_lowest(Card("K")))
        self.assertEqual(self.trick4.cards(), ["J", "Q", "K"])
        self.assertFalse(self.trick4.play_normal(Card("10")))
        self.assertTrue(self.trick4.play_normal(Card("A")))
        self.assertEqual(self.trick4.cards(), ["J", "Q", "K", "A"])

        self.assertEqual(self.trick4.winner(), 1)

    def test_reset(self):
        self.trick2.play_normal(Card("5"))
        self.trick2.play_normal(Card("J"))
        self.assertEqual(self.trick2.cards(), ["5", "J"])
        self.assertEqual(self.trick2.winner(), 1)
        self.trick2.reset()

        self.assertEqual(self.trick2.cards(), [])
        self.trick2.play_normal(Card("5"))
        self.assertEqual(self.trick2.cards(), ["5"])
        self.trick2.play_as_lowest(Card("2"))
        self.assertEqual(self.trick2.winner(), 0)
        self.assertEqual(self.trick2.cards(), ["5", "2"])

    def test_size(self):
        self.assertEqual(self.trick2.size(), 0)
        self.trick2.play_normal(Card("5"))
        self.assertEqual(self.trick2.size(), 1)
        self.trick2.play_normal(Card("J"))
        self.assertEqual(self.trick2.size(), 2)
        self.trick2.reset()

        self.assertEqual(self.trick2.size(), 0)
        self.trick2.play_normal(Card("5"))
        self.assertEqual(self.trick2.size(), 1)


class TestGame(unittest.TestCase):
    def setUp(self):
        self.state = GameState(2)
        cards1 = ["2", "3", "5", "4", "J", "A"]
        cards2 = ["3", "7", "Q", "5", "K", "2"]
        for card_str in cards1:
            self.state.add_card(0, card_str)
        for card_str in cards2:
            self.state.add_card(1, card_str)
        self.game = Game(2, 0, [Hand(cards1, CardFactory(Card)),
                                Hand(cards2, CardFactory(Card))],
                         Trick(0, 2), self.state)

    def test_game(self):
        self.assertFalse(self.game.play(1, 0))

        self.assertTrue(self.game.play(0, 2))
        self.assertEqual(self.game.state().get_cards(0), ["2", "3", "", "4", "J", "A"])
        self.assertEqual(self.game.current_player, 1)

        self.assertTrue(self.game.play(1, 4))
        self.assertEqual(self.game.state().get_cards(1), ["3", "7", "Q", "5", "", "2"])
        self.assertEqual(self.game.state().get_trick(0), "")
        self.assertEqual(self.game.state().get_trick(1), "")

        self.assertTrue(self.game.play(1, 1))
        self.assertEqual(self.game.state().get_trick(1), "7")


class TestDealer(unittest.TestCase):
    def setUp(self):
        self.dealer = Dealer(3, Hand, Game, Card, Trick, CardFactory, GameState)

    def test_game_type(self):
        self.assertEqual(self.dealer.game_type(), "normal")

    def test_create_game(self):
        game = self.dealer.create_game()
        self.assertEqual(len(game.state().get_cards(0)), 6)
        self.assertTrue("" not in game.state().get_cards(1))
        game.play(0, 0)


class TestGameServer(unittest.TestCase):
    def setUp(self):
        self.dealer = Dealer(3, Hand, Game, Card, Trick, CardFactory, GameState)
        self.game_server = GameServer(self.dealer)

    def test_new_game(self):
        self.assertTrue(self.game_server.new_game("normal"))
        self.assertFalse(self.game_server.new_game(""))
        self.assertFalse(self.game_server.new_game("normal2"))

    def test_play(self):
        self.game_server.new_game("normal")
        self.assertTrue(self.game_server.play(0, 1))
        self.assertEqual(self.game_server.state(1).get_cards(0).count("?"), 5)
        self.assertTrue("" in self.game_server.state(1).get_cards(0))
        self.assertNotEqual(self.game_server.state(1).get_trick(0), "")
        self.assertFalse(self.game_server.play(0, 4))
        self.assertFalse(self.game_server.play(2, 5))

    def test_change_dealer(self):
        self.game_server.change_dealer(Dealer(2, Hand, Game, Card, Trick, CardFactory, GameState))
        self.assertTrue(self.game_server.new_game("normal"))


class TestSubsetCard(unittest.TestCase):
    def setUp(self):
        self.card1 = SubsetCard("1, 2, 3, 4, 5, 6, 7")
        self.card2 = SubsetCard("3, 4, 2, 1, 5, 7, 6")
        self.card3 = SubsetCard("1, 3, 2")
        self.card4 = SubsetCard("4")

    def test_string(self):
        self.assertEqual(self.card1.string(), "1, 2, 3, 4, 5, 6, 7")
        self.assertEqual(self.card2.string(), "3, 4, 2, 1, 5, 7, 6")
        self.assertEqual(self.card3.string(), "1, 3, 2")
        self.assertEqual(self.card4.string(), "4")

    def test_is_ge(self):
        self.assertTrue(self.card1.is_ge(self.card2))
        self.assertTrue(self.card2.is_ge(self.card1))
        self.assertTrue(self.card1.is_ge(self.card3))
        self.assertFalse(self.card3.is_ge(self.card1))
        self.assertFalse(self.card4.is_ge(self.card3))
        self.assertFalse(self.card3.is_ge(self.card4))


class TestSubsetDealer(unittest.TestCase):
    def setUp(self):
        self.dealer = SubsetDealer(3, Hand, Game, SubsetCard, Trick, CardFactory, GameState)

    def test_game_type(self):
        self.assertEqual(self.dealer.game_type(), "subset")

    def test_create_game(self):
        game = self.dealer.create_game()
        self.assertEqual(len(game.state().get_cards(0)), 6)
        self.assertTrue("" not in game.state().get_cards(1))
        game.play(0, 0)


class TestEqualTrick(unittest.TestCase):
    def setUp(self):
        self.trick2 = EqualTrick(0, 2)
        self.hand2_0 = Hand(["2", "5", "J", "", "", ""], CardFactory(Card))
        self.hand2_1 = Hand(["J", "K", "3", "", "", ""], CardFactory(Card))

    def test_trick(self):
        self.assertTrue(self.trick2.play_normal(Card("J")))
        self.assertTrue(self.trick2.play_normal(Card("J")))
        self.assertEqual(self.trick2.winner(), 0)


class TestSubsetGame(unittest.TestCase):
    def setUp(self):
        self.state = GameState(2)
        cards1 = ["0, 1, 2", "5", "1, 2", "0, 1, 2, 3, 4", "7", "5, 6"]
        cards2 = ["0, 1", "3", "4, 6, 1", "2, 1", "2, 1, 0", "5"]
        for card_str in cards1:
            self.state.add_card(0, card_str)
        for card_str in cards2:
            self.state.add_card(1, card_str)
        self.game = Game(2, 0, [Hand(cards1, CardFactory(SubsetCard)),
                                Hand(cards2, CardFactory(SubsetCard))],
                         Trick(0, 2), self.state)

    def test_game(self):
        self.assertFalse(self.game.play(1, 0))

        self.assertTrue(self.game.play(0, 2))
        self.assertEqual(self.game.state().get_cards(0), ["0, 1, 2", "5", "", "0, 1, 2, 3, 4", "7", "5, 6"])
        self.assertEqual(self.game.current_player, 1)

        self.assertTrue(self.game.play(1, 5))
        self.assertEqual(self.game.state().get_cards(1), ["0, 1", "3", "4, 6, 1", "2, 1", "2, 1, 0", ""])
        self.assertEqual(self.game.state().get_trick(0), "")
        self.assertEqual(self.game.state().get_trick(1), "")

        self.assertTrue(self.game.play(1, 1))
        self.assertEqual(self.game.state().get_trick(1), "3")


if __name__ == "__main__":
    unittest.main()
