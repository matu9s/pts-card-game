import unittest

from game.GameState import GameState


class TestGameState(unittest.TestCase):
    def setUp(self):
        self.game_state = GameState(4)
        self.game_state.add_card(0, "J")
        self.game_state.add_card(0, "5")
        self.game_state.add_card(3, "3")
        self.game_state.add_card(3, "3")
        self.game_state.add_card(3, "3")
        self.game_state.add_card(3, "J")
        self.game_state.add_card(3, "K")
        self.game_state.add_card(3, "10")
        self.game_state.add_card(1, "2")
        for i in range(6):
            self.game_state.add_card(2, "?")
        self.game_state.set_trick(0, "Q")
        self.game_state.set_trick(1, "10")

    def test_initialization(self):
        empty_state = GameState(3)
        for i in range(3):
            self.assertEqual(empty_state.get_cards(i), ["" for _ in range(6)])
            self.assertEqual(empty_state.get_trick(i), "")

    def test_setUp(self):
        self.assertTrue("J" in self.game_state.get_cards(0))
        self.assertTrue("5" in self.game_state.get_cards(0))
        self.assertEqual(self.game_state.get_cards(3).count("3"), 3)
        self.assertFalse("2" in self.game_state.get_cards(0))
        self.assertFalse("3" in self.game_state.get_cards(0))
        self.assertEqual(self.game_state.get_cards(2), ["?" for _ in range(6)])
        self.assertEqual(self.game_state.get_trick(0), "Q")
        self.assertEqual(self.game_state.get_trick(1), "10")
        self.assertEqual(self.game_state.get_trick(2), "")

    def test_add_card(self):
        self.game_state.add_card(0, "7")
        self.assertTrue("7" in self.game_state.get_cards(0))

    def test_remove_card(self):
        self.game_state.remove_card(3, "K")
        self.assertFalse("K" in self.game_state.get_cards(3))
        self.game_state.remove_card(3, "3")
        self.assertEqual(self.game_state.get_cards(3).count("3"), 2)

    def test_trick(self):
        self.game_state.set_trick(0, "5")
        self.assertEqual(self.game_state.get_trick(0), "5")

    def test_set_others_unknown(self):
        self.game_state.set_others_unknown(3)
        self.assertEqual(self.game_state.get_cards(0), ["?", "?", "", "", "", ""])
        self.assertEqual(self.game_state.get_cards(3), ["3", "3", "3", "J", "K", "10"])
        self.assertEqual(self.game_state.get_cards(2), ["?" for _ in range(6)])


if __name__ == "__main__":
    unittest.main()
