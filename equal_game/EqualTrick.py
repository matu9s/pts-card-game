from game.Trick import Trick
from game_interfaces.CardInterface import CardInterface


class EqualTrick(Trick):
    def play_normal(self, chosen_card: CardInterface) -> bool:
        change_winner = True
        for card in self._cards:
            if not chosen_card.is_ge(card):
                return False
            if card.is_ge(chosen_card):
                change_winner = False
        if change_winner:
            self._possible_winner = self._current_player
        self._cards.append(chosen_card)
        self._next_player()
        return True
