from typing import List

from game_interfaces.GameStateInterface import GameStateInterface
from game_interfaces.HandInterface import HandInterface
from game_interfaces.TrickInterface import TrickInterface


class GameInterface:
    def __init__(self, number_of_players: int, trick_started_by: int,
                 hands: List[HandInterface], trick: TrickInterface,
                 game_state: GameStateInterface):
        pass

    def play(self, player: int, cardno: int) -> bool:
        pass

    def state(self) -> GameStateInterface:
        pass
