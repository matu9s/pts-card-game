from game_interfaces.GameStateInterface import GameStateInterface


class GameServerInterface:
    def play(self, player: int, cardno: int) -> bool:
        pass

    def state(self, player: int) -> GameStateInterface:
        pass

    def new_game(self, type: str) -> bool:
        pass
