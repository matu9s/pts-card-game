from typing import List


class GameStateInterface:
    def __init__(self, number_of_players: int):
        pass

    def add_card(self, player: int, card: str):
        pass

    def remove_card(self, player: int, card: str):
        pass

    def set_trick(self, player: int, card: str):
        pass

    def get_trick(self, player: int):
        pass

    def get_cards(self, player: int) -> List[str]:
        pass

    def set_others_unknown(self, player: int):
        pass
