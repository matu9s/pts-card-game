from typing import List

from game_interfaces.CardInterface import CardInterface


class TrickInterface:
    def __init__(self, starting_player: int, number_of_players: int):
        pass

    def play_as_lowest(self, card: CardInterface) -> bool:
        pass

    def play_normal(self, card: CardInterface) -> bool:
        pass

    def winner(self) -> int:
        pass

    def cards(self) -> List[str]:
        pass

    def size(self) -> int:
        pass

    def reset(self):
        pass
