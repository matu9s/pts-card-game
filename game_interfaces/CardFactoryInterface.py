from typing import Type

from game_interfaces.CardInterface import CardInterface


class CardFactoryInterface:
    def __init__(self, card_type: Type[CardInterface]):
        pass

    def create(self, card_name: str) -> CardInterface:
        pass
