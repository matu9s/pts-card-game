from typing import List

from game_interfaces.CardFactoryInterface import CardFactoryInterface
from game_interfaces.TrickInterface import TrickInterface


class HandInterface:
    def __init__(self, card_list: List[str], card_factory: CardFactoryInterface):
        pass

    def play(self, cardno: int, trick: TrickInterface) -> bool:
        pass

    def cards(self) -> List[str]:
        pass
