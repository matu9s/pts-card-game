from game_interfaces.GameInterface import GameInterface
from game_interfaces.TrickInterface import TrickInterface


class DealerInterface:
    def create_game(self) -> GameInterface:
        pass

    def game_type(self) -> str:
        pass

    def create_trick(self) -> TrickInterface:
        pass
